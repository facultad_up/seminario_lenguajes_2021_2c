from antlr4 import InputStream, CommonTokenStream, ParseTreeWalker

from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener


class AritmeticaVisitor(AritmeticaListener):

  def __init__(self):
    self.variables = {}

  def enterProgram(self, ctx):    # ctx es un nodo de program
    for node in ctx.children:
      self.visit_statement(node)

  def visit_statement(self, ctx): # ctx es un nodo de statement
    child_node = ctx.children[0]  # tomando el unico nodo hijo
    if type(child_node) == AritmeticaParser.Assign_statementContext:
      self.visit_assign_statement(child_node)
    elif type(child_node) == AritmeticaParser.ExpressionContext:
      value = self.visit_expression_statement(child_node)
      print('The result is:', value)
    
  def visit_assign_statement(self, ctx): # ctx es nodo de assign_statement
    variable_name = ctx.children[0].symbol.text
    value = self.visit_expression_statement(ctx.children[2])
    self.variables[variable_name] = value

  def visit_expression_statement(self, ctx): # ctx es nodo expression
    value = 0

    if type(ctx.children[0]) == AritmeticaParser.TermContext:
      value = self.visit_term(ctx.children[0])
    else:
      value1 = self.visit_expression_statement(ctx.children[0])
      value2 = self.visit_term(ctx.children[2])
      if ctx.children[1].symbol.text == '+':
        value = value1 + value2
      else:
        value = value1 - value2

    return value

  def visit_term(self, ctx): # ctx es nodo term
    value = 0
    if type(ctx.children[0]) == AritmeticaParser.FactorContext:
      value = self.visit_factor(ctx.children[0])
    else:
      value1 = self.visit_term(ctx.children[0])
      value2 = self.visit_factor(ctx.children[2])
      if ctx.children[1].symbol.text == '*':
        value = value1 * value2
      elif ctx.children[1].symbol.text == '/':
        value = value1 / value2
      elif ctx.children[1].symbol.text == '^':
        value = value1 ^ value2

    return value

  def visit_factor(self, ctx): # ctx es nodo factor
    value = 0
    if len(ctx.children) == 1:
      if ctx.var:
        value = self.variables[ctx.var.text]
      else:
        value = int(ctx.num.text)
    else:
      value = self.visit_expression_statement(ctx.children[1])

    return value



programa = '''
aux <- (5+13*8)
2*aux+100
'''

ingreso = InputStream(programa)
lexer = AritmeticaLexer(ingreso)
token_stream = CommonTokenStream(lexer)
parser = AritmeticaParser(token_stream)

tree = parser.program()

print('Empezando a visitar los nodos del arbol:')
print('-' * 60)
visitor = AritmeticaVisitor()
walker = ParseTreeWalker()
walker.walk(visitor, tree)
