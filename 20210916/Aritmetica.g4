grammar Aritmetica;

program:
  (statement)+
  ;

statement:
    expression
  | assign_statement
  //| if_statement
  ;

assign_statement:
  VARIABLE '<-' expression
  ;

expression:
    term
  | expression '+' term
  | expression '-' term
  ;

term:
    factor
  | term '*' factor
  | term '/' factor
  | term '^' factor
  ;

factor:
    num=NUMBER
  | var=VARIABLE
  | '(' expression ')'
  ;

VARIABLE : [a-zA-Z]+[a-zA-Z0-9]*;
NUMBER : [0-9]+ ;
BLANK : [ \r\n\t] -> skip ;
