# Generated from Aritmetica.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\r")
        buf.write("C\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2")
        buf.write("\6\2\20\n\2\r\2\16\2\21\3\3\3\3\5\3\26\n\3\3\4\3\4\3\4")
        buf.write("\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\7\5%\n\5\f\5")
        buf.write("\16\5(\13\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3")
        buf.write("\6\3\6\7\6\66\n\6\f\6\16\69\13\6\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\5\7A\n\7\3\7\2\4\b\n\b\2\4\6\b\n\f\2\2\2E\2\17\3\2")
        buf.write("\2\2\4\25\3\2\2\2\6\27\3\2\2\2\b\33\3\2\2\2\n)\3\2\2\2")
        buf.write("\f@\3\2\2\2\16\20\5\4\3\2\17\16\3\2\2\2\20\21\3\2\2\2")
        buf.write("\21\17\3\2\2\2\21\22\3\2\2\2\22\3\3\2\2\2\23\26\5\b\5")
        buf.write("\2\24\26\5\6\4\2\25\23\3\2\2\2\25\24\3\2\2\2\26\5\3\2")
        buf.write("\2\2\27\30\7\13\2\2\30\31\7\3\2\2\31\32\5\b\5\2\32\7\3")
        buf.write("\2\2\2\33\34\b\5\1\2\34\35\5\n\6\2\35&\3\2\2\2\36\37\f")
        buf.write("\4\2\2\37 \7\4\2\2 %\5\n\6\2!\"\f\3\2\2\"#\7\5\2\2#%\5")
        buf.write("\n\6\2$\36\3\2\2\2$!\3\2\2\2%(\3\2\2\2&$\3\2\2\2&\'\3")
        buf.write("\2\2\2\'\t\3\2\2\2(&\3\2\2\2)*\b\6\1\2*+\5\f\7\2+\67\3")
        buf.write("\2\2\2,-\f\5\2\2-.\7\6\2\2.\66\5\f\7\2/\60\f\4\2\2\60")
        buf.write("\61\7\7\2\2\61\66\5\f\7\2\62\63\f\3\2\2\63\64\7\b\2\2")
        buf.write("\64\66\5\f\7\2\65,\3\2\2\2\65/\3\2\2\2\65\62\3\2\2\2\66")
        buf.write("9\3\2\2\2\67\65\3\2\2\2\678\3\2\2\28\13\3\2\2\29\67\3")
        buf.write("\2\2\2:A\7\f\2\2;A\7\13\2\2<=\7\t\2\2=>\5\b\5\2>?\7\n")
        buf.write("\2\2?A\3\2\2\2@:\3\2\2\2@;\3\2\2\2@<\3\2\2\2A\r\3\2\2")
        buf.write("\2\t\21\25$&\65\67@")
        return buf.getvalue()


class AritmeticaParser ( Parser ):

    grammarFileName = "Aritmetica.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'<-'", "'+'", "'-'", "'*'", "'/'", "'^'", 
                     "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "VARIABLE", "NUMBER", "BLANK" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_assign_statement = 2
    RULE_expression = 3
    RULE_term = 4
    RULE_factor = 5

    ruleNames =  [ "program", "statement", "assign_statement", "expression", 
                   "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    VARIABLE=9
    NUMBER=10
    BLANK=11

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = AritmeticaParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 13 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 12
                self.statement()
                self.state = 15 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__6) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def assign_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.Assign_statementContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritmeticaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 19
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 17
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 18
                self.assign_statement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_statementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE(self):
            return self.getToken(AritmeticaParser.VARIABLE, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_assign_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign_statement" ):
                listener.enterAssign_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign_statement" ):
                listener.exitAssign_statement(self)




    def assign_statement(self):

        localctx = AritmeticaParser.Assign_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_assign_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 21
            self.match(AritmeticaParser.VARIABLE)
            self.state = 22
            self.match(AritmeticaParser.T__0)
            self.state = 23
            self.expression(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 6
        self.enterRecursionRule(localctx, 6, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 26
            self.term(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 36
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,3,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 34
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 28
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 29
                        self.match(AritmeticaParser.T__1)
                        self.state = 30
                        self.term(0)
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 31
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 32
                        self.match(AritmeticaParser.T__2)
                        self.state = 33
                        self.term(0)
                        pass

             
                self.state = 38
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,3,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class TermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self):
            return self.getTypedRuleContext(AritmeticaParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 8
        self.enterRecursionRule(localctx, 8, self.RULE_term, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 40
            self.factor()
            self._ctx.stop = self._input.LT(-1)
            self.state = 53
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 51
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 42
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 43
                        self.match(AritmeticaParser.T__3)
                        self.state = 44
                        self.factor()
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 45
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 46
                        self.match(AritmeticaParser.T__4)
                        self.state = 47
                        self.factor()
                        pass

                    elif la_ == 3:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 48
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 49
                        self.match(AritmeticaParser.T__5)
                        self.state = 50
                        self.factor()
                        pass

             
                self.state = 55
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class FactorContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.num = None # Token
            self.var = None # Token

        def NUMBER(self):
            return self.getToken(AritmeticaParser.NUMBER, 0)

        def VARIABLE(self):
            return self.getToken(AritmeticaParser.VARIABLE, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = AritmeticaParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_factor)
        try:
            self.state = 62
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 56
                localctx.num = self.match(AritmeticaParser.NUMBER)
                pass
            elif token in [AritmeticaParser.VARIABLE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 57
                localctx.var = self.match(AritmeticaParser.VARIABLE)
                pass
            elif token in [AritmeticaParser.T__6]:
                self.enterOuterAlt(localctx, 3)
                self.state = 58
                self.match(AritmeticaParser.T__6)
                self.state = 59
                self.expression(0)
                self.state = 60
                self.match(AritmeticaParser.T__7)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[3] = self.expression_sempred
        self._predicates[4] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 1)
         




