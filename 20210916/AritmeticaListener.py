# Generated from Aritmetica.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .AritmeticaParser import AritmeticaParser
else:
    from AritmeticaParser import AritmeticaParser

# This class defines a complete listener for a parse tree produced by AritmeticaParser.
class AritmeticaListener(ParseTreeListener):

    # Enter a parse tree produced by AritmeticaParser#program.
    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#program.
    def exitProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#statement.
    def enterStatement(self, ctx:AritmeticaParser.StatementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#statement.
    def exitStatement(self, ctx:AritmeticaParser.StatementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#assign_statement.
    def enterAssign_statement(self, ctx:AritmeticaParser.Assign_statementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#assign_statement.
    def exitAssign_statement(self, ctx:AritmeticaParser.Assign_statementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#expression.
    def enterExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#expression.
    def exitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#term.
    def enterTerm(self, ctx:AritmeticaParser.TermContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#term.
    def exitTerm(self, ctx:AritmeticaParser.TermContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#factor.
    def enterFactor(self, ctx:AritmeticaParser.FactorContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#factor.
    def exitFactor(self, ctx:AritmeticaParser.FactorContext):
        pass



del AritmeticaParser