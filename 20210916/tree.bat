@echo off

rem Param 1: Nombre gramatica
rem Param 2: Punto de entrada
rem Param 3: El programa 

echo Recordar: export CLASSPATH="../antlr49.jar"

echo GENERANDO LEXER Y PARSER EN JAVA...
java -jar ../antlr49.jar %1.g4

echo COMPILANDO ARCHIVOS JAVA...

javac -g *.java
rem javac -cp .;../antlr-4.9.2.jar -g *.java


echo GENERANDO ARBOL...
java -cp ".;../antlr49.jar" org.antlr.v4.gui.TestRig %1 %2 -gui %3

echo LIMPIANDO ARCHIVOS...
del *.java
del *.class