from lexer import Lexer, LexerException, TokenType



class AST():
  pass


class BinaryOpNode(AST):
  def __init__(self, left, op, right):
    self.left = left
    self.op = op
    self.right = right


class NumberNode(AST):
  def __init__(self, token):
    self.token = token


class Parser():

  def __init__(self, lexer):
    self.lexer = lexer
    self.current_token = lexer.next_token()

  def __consume__(self, token_type):
    if self.current_token.token_type == token_type:
      self.current_token = self.lexer.next_token()
    else:
      raise Exception('Invalid syntax.')

  '''
  factor:
    NUMBER | '(' expression ')'
  '''
  def __factor__(self):
    token = self.current_token

    if token.token_type == TokenType.NUMBER:
      self.__consume__(TokenType.NUMBER)
      return NumberNode(token)

    if token.token_type == TokenType.LPAREN:
      self.__consume__(TokenType.LPAREN)
      node = self.__expression__()
      self.__consume__(TokenType.RPAREN)
      return node

  '''
  term:
    factor ((MULTIPLY | DIVIDE) factor)*
  '''
  def __term__(self):
    node = self.__factor__()
    
    while self.current_token != None and self.current_token.token_type in (TokenType.DIVIDE, TokenType.MULTIPLY):
      token = self.current_token
      if token.token_type == TokenType.MULTIPLY:
        self.__consume__(TokenType.MULTIPLY)
      elif token.token_type == TokenType.DIVIDE:
        self.__consume__(TokenType.DIVIDE)

      node = BinaryOpNode(node, token, self.__factor__())

    return node

  '''
  expression:
    term ((PLUS | MINUS) term)*
  '''
  def __expression__(self):
    node = self.__term__()
    
    while self.current_token != None and self.current_token.token_type in (TokenType.PLUS, TokenType.MINUS):
      token = self.current_token
      if token.token_type == TokenType.PLUS:
        self.__consume__(TokenType.PLUS)
      elif token.token_type == TokenType.MINUS:
        self.__consume__(TokenType.MINUS)

      node = BinaryOpNode(node, token, self.__term__())
    
    return node

  def parse(self):
    return self.__expression__()


rules = [
  ('\d+',                       TokenType.NUMBER),
  ('[a-zA-Z]+[a-zA-Z0-9]*\w+',  TokenType.IDENTIFIER),
  ('\+',                        TokenType.PLUS),
  ('\-',                        TokenType.MINUS),
  ('\=',                        TokenType.ASSIGN),
  ('\*',                        TokenType.MULTIPLY),
  ('\/',                        TokenType.DIVIDE),
  ('\(',                        TokenType.LPAREN),
  ('\)',                        TokenType.RPAREN),
]

lexer = Lexer(rules)
lexer.input('4/2+6*3')

parser = Parser(lexer)

ast = parser.parse()

print('Fin.')
