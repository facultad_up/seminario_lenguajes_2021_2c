import re, enum

class TokenType(enum.Enum):
  NUMBER = 0
  IDENTIFIER = 1
  PLUS = 2
  MINUS = 3
  ASSIGN = 4
  MULTIPLY = 5
  DIVIDE = 6
  LPAREN = 7
  RPAREN = 8

class LexerException(Exception):
  def __init__(self, position):
    self.position = position

class Token():
  def __init__(self, token_type, value, position):
    self.token_type = token_type
    self.value = value
    self.position = position
  
  def __str__(self):
    return '{} = {} in position {}'.format(self.token_type, self.value, self.position)
    
class Lexer():
  def __init__(self, rules):
    self.rules = rules
    self.group_type = {}

    index = 1
    regex_parts = []

    for regex, token_type in rules:
      groupname = 'GROUP{}'.format(index)
      regex_parts.append('(?P<{}>{})'.format(groupname, regex))
      self.group_type[groupname] = token_type

      index += 1

    self.regex = re.compile('|'.join(regex_parts))
    self.regex_ws = re.compile('\S')

  def input(self, buffer):
    self.buffer = buffer
    self.position = 0

  def next_token(self):
    if self.position < len(self.buffer):
      m = self.regex_ws.search(self.buffer, self.position)
      if m:
        self.position = m.start()
      
      m = self.regex.match(self.buffer, self.position)
      if m:
        groupname = m.lastgroup
        token_type = self.group_type[groupname]
        token = Token(token_type, m.group(groupname), self.position)
        self.position = m.end()
        return token

      raise LexerException(self.position)

    else:
      return None

  def tokens(self):
    while True:
      token = self.next_token()
      if token is None:
        break

      yield token
