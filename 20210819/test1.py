from antlr4 import InputStream, CommonTokenStream

from GramaticaHolaMundoLexer import GramaticaHolaMundoLexer
from GramaticaHolaMundoParser import GramaticaHolaMundoParser

programa = 'hola Gabriel'


ingreso = InputStream(programa)
lexer = GramaticaHolaMundoLexer(ingreso)
token_stream = CommonTokenStream(lexer)
parser = GramaticaHolaMundoParser(token_stream)

t = parser.saludar()
