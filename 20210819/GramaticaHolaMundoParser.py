# Generated from GramaticaHolaMundo.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\4")
        buf.write("\b\4\2\t\2\3\2\3\2\3\2\3\2\2\2\3\2\2\2\2\6\2\4\3\2\2\2")
        buf.write("\4\5\7\3\2\2\5\6\7\4\2\2\6\3\3\2\2\2\2")
        return buf.getvalue()


class GramaticaHolaMundoParser ( Parser ):

    grammarFileName = "GramaticaHolaMundo.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'hola '" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "NOMBRE" ]

    RULE_saludar = 0

    ruleNames =  [ "saludar" ]

    EOF = Token.EOF
    T__0=1
    NOMBRE=2

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class SaludarContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NOMBRE(self):
            return self.getToken(GramaticaHolaMundoParser.NOMBRE, 0)

        def getRuleIndex(self):
            return GramaticaHolaMundoParser.RULE_saludar

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSaludar" ):
                listener.enterSaludar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSaludar" ):
                listener.exitSaludar(self)




    def saludar(self):

        localctx = GramaticaHolaMundoParser.SaludarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_saludar)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2
            self.match(GramaticaHolaMundoParser.T__0)
            self.state = 3
            self.match(GramaticaHolaMundoParser.NOMBRE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





