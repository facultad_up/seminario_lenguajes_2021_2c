# Generated from GramaticaHolaMundo.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\4")
        buf.write("\23\b\1\4\2\t\2\4\3\t\3\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3")
        buf.write("\3\6\3\20\n\3\r\3\16\3\21\2\2\4\3\3\5\4\3\2\4\3\2C\\\3")
        buf.write("\2c|\2\23\2\3\3\2\2\2\2\5\3\2\2\2\3\7\3\2\2\2\5\r\3\2")
        buf.write("\2\2\7\b\7j\2\2\b\t\7q\2\2\t\n\7n\2\2\n\13\7c\2\2\13\f")
        buf.write("\7\"\2\2\f\4\3\2\2\2\r\17\t\2\2\2\16\20\t\3\2\2\17\16")
        buf.write("\3\2\2\2\20\21\3\2\2\2\21\17\3\2\2\2\21\22\3\2\2\2\22")
        buf.write("\6\3\2\2\2\4\2\21\2")
        return buf.getvalue()


class GramaticaHolaMundoLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    NOMBRE = 2

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'hola '" ]

    symbolicNames = [ "<INVALID>",
            "NOMBRE" ]

    ruleNames = [ "T__0", "NOMBRE" ]

    grammarFileName = "GramaticaHolaMundo.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


