# Generated from GramaticaHolaMundo.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .GramaticaHolaMundoParser import GramaticaHolaMundoParser
else:
    from GramaticaHolaMundoParser import GramaticaHolaMundoParser

# This class defines a complete listener for a parse tree produced by GramaticaHolaMundoParser.
class GramaticaHolaMundoListener(ParseTreeListener):

    # Enter a parse tree produced by GramaticaHolaMundoParser#saludar.
    def enterSaludar(self, ctx:GramaticaHolaMundoParser.SaludarContext):
        pass

    # Exit a parse tree produced by GramaticaHolaMundoParser#saludar.
    def exitSaludar(self, ctx:GramaticaHolaMundoParser.SaludarContext):
        pass



del GramaticaHolaMundoParser