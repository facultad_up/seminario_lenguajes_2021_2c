grammar Aritmetica;

// (3+10)*5
// 5
// 6*2
// 2*4+6*(5+3*8)

// EXPRESION -- TERMINOS ( + - ) -- FACTORES (* / ^)

statement:
  e=expression              //{print("Resultado: ", $e.value) }
  ;

expression returns [int value]:
    t=term                  //{$value = $t.value              }   
  | e=expression '+' t=term //{$value = $e.value + $t.value   }         
  | e=expression '-' t=term //{$value = $e.value - $t.value   }         
  ;

term returns [int value]:
    f=factor                //{$value = $f.value              }   
  | t=term '*' f=factor     //{$value = $t.value * $f.value   }         
  | t=term '/' f=factor     //{$value = $t.value / $f.value   }         
  | t=term '^' f=factor     //{$value = $t.value ** $f.value  }         
  ;

factor returns [int value]:
    NUMBER                  //{$value = int($NUMBER.text) }
  | '(' e=expression ')'    //{$value = $e.value          }
  ;


NUMBER : [0-9]+ ;
BLANK : [ \r\n\t] -> skip ;
