# Generated from GramaticaHolaMundo.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\7")
        buf.write("\37\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\6\2\f\n\2\r\2")
        buf.write("\16\2\r\3\3\3\3\5\3\22\n\3\3\4\3\4\3\4\3\4\3\4\5\4\31")
        buf.write("\n\4\3\5\3\5\3\5\3\5\3\5\2\2\6\2\4\6\b\2\2\2\35\2\13\3")
        buf.write("\2\2\2\4\21\3\2\2\2\6\30\3\2\2\2\b\32\3\2\2\2\n\f\5\4")
        buf.write("\3\2\13\n\3\2\2\2\f\r\3\2\2\2\r\13\3\2\2\2\r\16\3\2\2")
        buf.write("\2\16\3\3\2\2\2\17\22\5\6\4\2\20\22\5\b\5\2\21\17\3\2")
        buf.write("\2\2\21\20\3\2\2\2\22\5\3\2\2\2\23\24\7\3\2\2\24\25\7")
        buf.write("\6\2\2\25\31\b\4\1\2\26\27\7\4\2\2\27\31\b\4\1\2\30\23")
        buf.write("\3\2\2\2\30\26\3\2\2\2\31\7\3\2\2\2\32\33\7\5\2\2\33\34")
        buf.write("\7\6\2\2\34\35\b\5\1\2\35\t\3\2\2\2\5\r\21\30")
        return buf.getvalue()


class GramaticaHolaMundoParser ( Parser ):

    grammarFileName = "GramaticaHolaMundo.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'hola '", "'chau'", "'insultar '" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "NOMBRE", "BLANK" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_saludar = 2
    RULE_insultar = 3

    ruleNames =  [ "program", "statement", "saludar", "insultar" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    NOMBRE=4
    BLANK=5

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(GramaticaHolaMundoParser.StatementContext)
            else:
                return self.getTypedRuleContext(GramaticaHolaMundoParser.StatementContext,i)


        def getRuleIndex(self):
            return GramaticaHolaMundoParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = GramaticaHolaMundoParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 9 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 8
                self.statement()
                self.state = 11 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << GramaticaHolaMundoParser.T__0) | (1 << GramaticaHolaMundoParser.T__1) | (1 << GramaticaHolaMundoParser.T__2))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def saludar(self):
            return self.getTypedRuleContext(GramaticaHolaMundoParser.SaludarContext,0)


        def insultar(self):
            return self.getTypedRuleContext(GramaticaHolaMundoParser.InsultarContext,0)


        def getRuleIndex(self):
            return GramaticaHolaMundoParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = GramaticaHolaMundoParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 15
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [GramaticaHolaMundoParser.T__0, GramaticaHolaMundoParser.T__1]:
                self.enterOuterAlt(localctx, 1)
                self.state = 13
                self.saludar()
                pass
            elif token in [GramaticaHolaMundoParser.T__2]:
                self.enterOuterAlt(localctx, 2)
                self.state = 14
                self.insultar()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SaludarContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._NOMBRE = None # Token

        def NOMBRE(self):
            return self.getToken(GramaticaHolaMundoParser.NOMBRE, 0)

        def getRuleIndex(self):
            return GramaticaHolaMundoParser.RULE_saludar

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSaludar" ):
                listener.enterSaludar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSaludar" ):
                listener.exitSaludar(self)




    def saludar(self):

        localctx = GramaticaHolaMundoParser.SaludarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_saludar)
        try:
            self.state = 22
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [GramaticaHolaMundoParser.T__0]:
                self.enterOuterAlt(localctx, 1)
                self.state = 17
                self.match(GramaticaHolaMundoParser.T__0)
                self.state = 18
                localctx._NOMBRE = self.match(GramaticaHolaMundoParser.NOMBRE)
                print("visita saludar", (None if localctx._NOMBRE is None else localctx._NOMBRE.text))      
                pass
            elif token in [GramaticaHolaMundoParser.T__1]:
                self.enterOuterAlt(localctx, 2)
                self.state = 20
                self.match(GramaticaHolaMundoParser.T__1)
                print("Chau.")               
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InsultarContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NOMBRE(self):
            return self.getToken(GramaticaHolaMundoParser.NOMBRE, 0)

        def getRuleIndex(self):
            return GramaticaHolaMundoParser.RULE_insultar

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInsultar" ):
                listener.enterInsultar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInsultar" ):
                listener.exitInsultar(self)




    def insultar(self):

        localctx = GramaticaHolaMundoParser.InsultarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_insultar)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 24
            self.match(GramaticaHolaMundoParser.T__2)
            self.state = 25
            self.match(GramaticaHolaMundoParser.NOMBRE)
            print("anda a la &$%&%")      
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





