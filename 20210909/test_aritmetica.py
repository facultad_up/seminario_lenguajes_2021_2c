from antlr4 import InputStream, CommonTokenStream

from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser

programa = '''
2*24+6*(5+13*8)
'''

ingreso = InputStream(programa)
lexer = AritmeticaLexer(ingreso)
token_stream = CommonTokenStream(lexer)
parser = AritmeticaParser(token_stream)

t = parser.statement()
