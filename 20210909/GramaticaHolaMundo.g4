grammar GramaticaHolaMundo;

// Reglas
program :
    (statement)+
  ;

statement :
    saludar
  | insultar
  ;

saludar:
    'hola ' NOMBRE      {print("visita saludar", $NOMBRE.text)      }
  | 'chau'              {print("Chau.")               }
  ;

insultar:
    'insultar ' NOMBRE  {print("anda a la &$%&%")      }
  ;


// Identificadores

NOMBRE : [A-Z][a-z]+ ;
BLANK: [ \r\n\t] -> skip;
