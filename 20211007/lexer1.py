import re
'''
VARIABLE : [a-zA-Z]+[a-zA-Z0-9]*
NUMBER : [0-9]+ 
OPERADORES: '='
'''
def generar_tokens(source):
  tokens = []

  words = source.split()
  
  for word in words:
    if re.match('^([0-9])*$', word):
      tokens.append(('NUMBER', word))      
    elif re.match('[a-zA-Z]+[a-zA-Z0-9]*', word):
      tokens.append(('VARIABLE', word))      
    elif word in '=*+-/%':
      tokens.append(('OPERATOR', word))      
    else:
      raise Exception('Token no valido: ' + word)
  return tokens

tokens = generar_tokens('a = 15\n b = a + 10')
print(tokens)