# Generated from Aritmetica.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\25")
        buf.write("u\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\3\2\6\2\26\n\2\r\2\16\2\27\3\3\3")
        buf.write("\3\3\3\3\3\5\3\36\n\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5")
        buf.write("\6\5(\n\5\r\5\16\5)\3\5\3\5\3\5\3\5\3\5\3\5\6\5\62\n\5")
        buf.write("\r\5\16\5\63\3\5\3\5\6\58\n\5\r\5\16\59\3\5\3\5\5\5>\n")
        buf.write("\5\3\6\3\6\3\6\3\6\6\6D\n\6\r\6\16\6E\3\6\3\6\3\7\3\7")
        buf.write("\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\bW\n\b")
        buf.write("\f\b\16\bZ\13\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\t\3\t\7\th\n\t\f\t\16\tk\13\t\3\n\3\n\3\n\3\n\3\n")
        buf.write("\3\n\5\ns\n\n\3\n\2\4\16\20\13\2\4\6\b\n\f\16\20\22\2")
        buf.write("\2\2{\2\25\3\2\2\2\4\35\3\2\2\2\6\37\3\2\2\2\b=\3\2\2")
        buf.write("\2\n?\3\2\2\2\fI\3\2\2\2\16M\3\2\2\2\20[\3\2\2\2\22r\3")
        buf.write("\2\2\2\24\26\5\4\3\2\25\24\3\2\2\2\26\27\3\2\2\2\27\25")
        buf.write("\3\2\2\2\27\30\3\2\2\2\30\3\3\2\2\2\31\36\5\16\b\2\32")
        buf.write("\36\5\6\4\2\33\36\5\b\5\2\34\36\5\n\6\2\35\31\3\2\2\2")
        buf.write("\35\32\3\2\2\2\35\33\3\2\2\2\35\34\3\2\2\2\36\5\3\2\2")
        buf.write("\2\37 \7\23\2\2 !\7\3\2\2!\"\5\16\b\2\"\7\3\2\2\2#$\7")
        buf.write("\4\2\2$%\5\f\7\2%\'\7\5\2\2&(\5\4\3\2\'&\3\2\2\2()\3\2")
        buf.write("\2\2)\'\3\2\2\2)*\3\2\2\2*+\3\2\2\2+,\7\6\2\2,>\3\2\2")
        buf.write("\2-.\7\4\2\2./\5\f\7\2/\61\7\5\2\2\60\62\5\4\3\2\61\60")
        buf.write("\3\2\2\2\62\63\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2\2\64")
        buf.write("\65\3\2\2\2\65\67\7\7\2\2\668\5\4\3\2\67\66\3\2\2\289")
        buf.write("\3\2\2\29\67\3\2\2\29:\3\2\2\2:;\3\2\2\2;<\7\6\2\2<>\3")
        buf.write("\2\2\2=#\3\2\2\2=-\3\2\2\2>\t\3\2\2\2?@\7\b\2\2@A\5\f")
        buf.write("\7\2AC\7\t\2\2BD\5\4\3\2CB\3\2\2\2DE\3\2\2\2EC\3\2\2\2")
        buf.write("EF\3\2\2\2FG\3\2\2\2GH\7\n\2\2H\13\3\2\2\2IJ\5\16\b\2")
        buf.write("JK\7\22\2\2KL\5\16\b\2L\r\3\2\2\2MN\b\b\1\2NO\5\20\t\2")
        buf.write("OX\3\2\2\2PQ\f\4\2\2QR\7\13\2\2RW\5\20\t\2ST\f\3\2\2T")
        buf.write("U\7\f\2\2UW\5\20\t\2VP\3\2\2\2VS\3\2\2\2WZ\3\2\2\2XV\3")
        buf.write("\2\2\2XY\3\2\2\2Y\17\3\2\2\2ZX\3\2\2\2[\\\b\t\1\2\\]\5")
        buf.write("\22\n\2]i\3\2\2\2^_\f\5\2\2_`\7\r\2\2`h\5\22\n\2ab\f\4")
        buf.write("\2\2bc\7\16\2\2ch\5\22\n\2de\f\3\2\2ef\7\17\2\2fh\5\22")
        buf.write("\n\2g^\3\2\2\2ga\3\2\2\2gd\3\2\2\2hk\3\2\2\2ig\3\2\2\2")
        buf.write("ij\3\2\2\2j\21\3\2\2\2ki\3\2\2\2ls\7\24\2\2ms\7\23\2\2")
        buf.write("no\7\20\2\2op\5\16\b\2pq\7\21\2\2qs\3\2\2\2rl\3\2\2\2")
        buf.write("rm\3\2\2\2rn\3\2\2\2s\23\3\2\2\2\16\27\35)\639=EVXgir")
        return buf.getvalue()


class AritmeticaParser ( Parser ):

    grammarFileName = "Aritmetica.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'<-'", "'if'", "'then'", "'fi'", "'else'", 
                     "'while'", "'do'", "'done'", "'+'", "'-'", "'*'", "'/'", 
                     "'^'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "COMPARATION_OPERATOR", "VARIABLE", "NUMBER", "BLANK" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_assign_statement = 2
    RULE_if_statement = 3
    RULE_while_statement = 4
    RULE_boolean_expression = 5
    RULE_expression = 6
    RULE_term = 7
    RULE_factor = 8

    ruleNames =  [ "program", "statement", "assign_statement", "if_statement", 
                   "while_statement", "boolean_expression", "expression", 
                   "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    COMPARATION_OPERATOR=16
    VARIABLE=17
    NUMBER=18
    BLANK=19

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = AritmeticaParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 19 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 18
                self.statement()
                self.state = 21 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__13) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def assign_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.Assign_statementContext,0)


        def if_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.If_statementContext,0)


        def while_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.While_statementContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritmeticaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 27
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 23
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 24
                self.assign_statement()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 25
                self.if_statement()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 26
                self.while_statement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_statementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE(self):
            return self.getToken(AritmeticaParser.VARIABLE, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_assign_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign_statement" ):
                listener.enterAssign_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign_statement" ):
                listener.exitAssign_statement(self)




    def assign_statement(self):

        localctx = AritmeticaParser.Assign_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_assign_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 29
            self.match(AritmeticaParser.VARIABLE)
            self.state = 30
            self.match(AritmeticaParser.T__0)
            self.state = 31
            self.expression(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_statementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def boolean_expression(self):
            return self.getTypedRuleContext(AritmeticaParser.Boolean_expressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_if_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_statement" ):
                listener.enterIf_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_statement" ):
                listener.exitIf_statement(self)




    def if_statement(self):

        localctx = AritmeticaParser.If_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_if_statement)
        self._la = 0 # Token type
        try:
            self.state = 59
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 33
                self.match(AritmeticaParser.T__1)
                self.state = 34
                self.boolean_expression()
                self.state = 35
                self.match(AritmeticaParser.T__2)
                self.state = 37 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 36
                    self.statement()
                    self.state = 39 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__13) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                        break

                self.state = 41
                self.match(AritmeticaParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 43
                self.match(AritmeticaParser.T__1)
                self.state = 44
                self.boolean_expression()
                self.state = 45
                self.match(AritmeticaParser.T__2)
                self.state = 47 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 46
                    self.statement()
                    self.state = 49 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__13) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                        break

                self.state = 51
                self.match(AritmeticaParser.T__4)
                self.state = 53 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 52
                    self.statement()
                    self.state = 55 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__13) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                        break

                self.state = 57
                self.match(AritmeticaParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class While_statementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def boolean_expression(self):
            return self.getTypedRuleContext(AritmeticaParser.Boolean_expressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_while_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhile_statement" ):
                listener.enterWhile_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhile_statement" ):
                listener.exitWhile_statement(self)




    def while_statement(self):

        localctx = AritmeticaParser.While_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_while_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self.match(AritmeticaParser.T__5)
            self.state = 62
            self.boolean_expression()
            self.state = 63
            self.match(AritmeticaParser.T__6)
            self.state = 65 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 64
                self.statement()
                self.state = 67 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__13) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 69
            self.match(AritmeticaParser.T__7)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Boolean_expressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,i)


        def COMPARATION_OPERATOR(self):
            return self.getToken(AritmeticaParser.COMPARATION_OPERATOR, 0)

        def getRuleIndex(self):
            return AritmeticaParser.RULE_boolean_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBoolean_expression" ):
                listener.enterBoolean_expression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBoolean_expression" ):
                listener.exitBoolean_expression(self)




    def boolean_expression(self):

        localctx = AritmeticaParser.Boolean_expressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_boolean_expression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 71
            self.expression(0)
            self.state = 72
            self.match(AritmeticaParser.COMPARATION_OPERATOR)
            self.state = 73
            self.expression(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 12
        self.enterRecursionRule(localctx, 12, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 76
            self.term(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 86
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 84
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 78
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 79
                        self.match(AritmeticaParser.T__8)
                        self.state = 80
                        self.term(0)
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 81
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 82
                        self.match(AritmeticaParser.T__9)
                        self.state = 83
                        self.term(0)
                        pass

             
                self.state = 88
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class TermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self):
            return self.getTypedRuleContext(AritmeticaParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 14
        self.enterRecursionRule(localctx, 14, self.RULE_term, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            self.factor()
            self._ctx.stop = self._input.LT(-1)
            self.state = 103
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,10,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 101
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 92
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 93
                        self.match(AritmeticaParser.T__10)
                        self.state = 94
                        self.factor()
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 95
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 96
                        self.match(AritmeticaParser.T__11)
                        self.state = 97
                        self.factor()
                        pass

                    elif la_ == 3:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 98
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 99
                        self.match(AritmeticaParser.T__12)
                        self.state = 100
                        self.factor()
                        pass

             
                self.state = 105
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,10,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class FactorContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.num = None # Token
            self.var = None # Token

        def NUMBER(self):
            return self.getToken(AritmeticaParser.NUMBER, 0)

        def VARIABLE(self):
            return self.getToken(AritmeticaParser.VARIABLE, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = AritmeticaParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_factor)
        try:
            self.state = 112
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 106
                localctx.num = self.match(AritmeticaParser.NUMBER)
                pass
            elif token in [AritmeticaParser.VARIABLE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 107
                localctx.var = self.match(AritmeticaParser.VARIABLE)
                pass
            elif token in [AritmeticaParser.T__13]:
                self.enterOuterAlt(localctx, 3)
                self.state = 108
                self.match(AritmeticaParser.T__13)
                self.state = 109
                self.expression(0)
                self.state = 110
                self.match(AritmeticaParser.T__14)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[6] = self.expression_sempred
        self._predicates[7] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 1)
         




