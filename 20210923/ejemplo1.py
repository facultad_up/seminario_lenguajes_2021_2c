from time import sleep

# Generadores (yield)

x = (i for i in range(10))
print(x.__next__())
print(x.__next__())
print(x.__next__())

def numeros(desde, hasta):
  print('comienza....')
  n = desde
  while n <= hasta:
    sleep(3)
    if n % 2 == 0:
      yield '{0} es par'.format(n)
    else:
      yield '{0} es impar'.format(n)

    print('Siguiente...')
    n += 1

  print('Fin.')

print(type(numeros(1, 10)))

# m = [l for l in numeros(1, 5)]
# print(m)

for l in numeros(1, 5):
  print(l)

# l = numeros(1, 5)
# d = l.__next__()
# while d != None:
#   print(d)
#   d = l.__next__()