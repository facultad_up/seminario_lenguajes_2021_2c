grammar Aritmetica;

program:
  (statement)+
  ;

statement:
    expression
  | assign_statement
  | if_statement
  | while_statement
  ;

assign_statement:
  VARIABLE '<-' expression
  ;

if_statement:
    'if' boolean_expression 'then' (statement)+ 'fi'
  | 'if' boolean_expression 'then' (statement)+ 'else' (statement)+ 'fi'
  ;

while_statement:
    'while' boolean_expression 'do' (statement)+ 'done'
  ;

// input statement

boolean_expression:
    expression COMPARATION_OPERATOR expression
  ;

expression:
    term
  | expression '+' term
  | expression '-' term
  ;

term:
    factor
  | term '*' factor
  | term '/' factor
  | term '^' factor
  ;

factor:
    num=NUMBER
  | var=VARIABLE
  | '(' expression ')'
  ;

COMPARATION_OPERATOR : '==' | '!=' | '>=' | '<=' | '>' | '<';
VARIABLE : [a-zA-Z]+[a-zA-Z0-9]*;
NUMBER : [0-9]+ ;
BLANK : [ \r\n\t] -> skip ;
