# Generated from Aritmetica.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\25")
        buf.write("w\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\13\3\13\3")
        buf.write("\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21b\n\21\3\22\6")
        buf.write("\22e\n\22\r\22\16\22f\3\22\7\22j\n\22\f\22\16\22m\13\22")
        buf.write("\3\23\6\23p\n\23\r\23\16\23q\3\24\3\24\3\24\3\24\2\2\25")
        buf.write("\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31")
        buf.write("\16\33\17\35\20\37\21!\22#\23%\24\'\25\3\2\7\4\2>>@@\4")
        buf.write("\2C\\c|\5\2\62;C\\c|\3\2\62;\5\2\13\f\17\17\"\"\2}\2\3")
        buf.write("\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2")
        buf.write("\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2")
        buf.write("\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2")
        buf.write("\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3")
        buf.write("\2\2\2\2\'\3\2\2\2\3)\3\2\2\2\5,\3\2\2\2\7/\3\2\2\2\t")
        buf.write("\64\3\2\2\2\13\67\3\2\2\2\r<\3\2\2\2\17B\3\2\2\2\21E\3")
        buf.write("\2\2\2\23J\3\2\2\2\25L\3\2\2\2\27N\3\2\2\2\31P\3\2\2\2")
        buf.write("\33R\3\2\2\2\35T\3\2\2\2\37V\3\2\2\2!a\3\2\2\2#d\3\2\2")
        buf.write("\2%o\3\2\2\2\'s\3\2\2\2)*\7>\2\2*+\7/\2\2+\4\3\2\2\2,")
        buf.write("-\7k\2\2-.\7h\2\2.\6\3\2\2\2/\60\7v\2\2\60\61\7j\2\2\61")
        buf.write("\62\7g\2\2\62\63\7p\2\2\63\b\3\2\2\2\64\65\7h\2\2\65\66")
        buf.write("\7k\2\2\66\n\3\2\2\2\678\7g\2\289\7n\2\29:\7u\2\2:;\7")
        buf.write("g\2\2;\f\3\2\2\2<=\7y\2\2=>\7j\2\2>?\7k\2\2?@\7n\2\2@")
        buf.write("A\7g\2\2A\16\3\2\2\2BC\7f\2\2CD\7q\2\2D\20\3\2\2\2EF\7")
        buf.write("f\2\2FG\7q\2\2GH\7p\2\2HI\7g\2\2I\22\3\2\2\2JK\7-\2\2")
        buf.write("K\24\3\2\2\2LM\7/\2\2M\26\3\2\2\2NO\7,\2\2O\30\3\2\2\2")
        buf.write("PQ\7\61\2\2Q\32\3\2\2\2RS\7`\2\2S\34\3\2\2\2TU\7*\2\2")
        buf.write("U\36\3\2\2\2VW\7+\2\2W \3\2\2\2XY\7?\2\2Yb\7?\2\2Z[\7")
        buf.write("#\2\2[b\7?\2\2\\]\7@\2\2]b\7?\2\2^_\7>\2\2_b\7?\2\2`b")
        buf.write("\t\2\2\2aX\3\2\2\2aZ\3\2\2\2a\\\3\2\2\2a^\3\2\2\2a`\3")
        buf.write("\2\2\2b\"\3\2\2\2ce\t\3\2\2dc\3\2\2\2ef\3\2\2\2fd\3\2")
        buf.write("\2\2fg\3\2\2\2gk\3\2\2\2hj\t\4\2\2ih\3\2\2\2jm\3\2\2\2")
        buf.write("ki\3\2\2\2kl\3\2\2\2l$\3\2\2\2mk\3\2\2\2np\t\5\2\2on\3")
        buf.write("\2\2\2pq\3\2\2\2qo\3\2\2\2qr\3\2\2\2r&\3\2\2\2st\t\6\2")
        buf.write("\2tu\3\2\2\2uv\b\24\2\2v(\3\2\2\2\7\2afkq\3\b\2\2")
        return buf.getvalue()


class AritmeticaLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    COMPARATION_OPERATOR = 16
    VARIABLE = 17
    NUMBER = 18
    BLANK = 19

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'<-'", "'if'", "'then'", "'fi'", "'else'", "'while'", "'do'", 
            "'done'", "'+'", "'-'", "'*'", "'/'", "'^'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "COMPARATION_OPERATOR", "VARIABLE", "NUMBER", "BLANK" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "COMPARATION_OPERATOR", "VARIABLE", "NUMBER", 
                  "BLANK" ]

    grammarFileName = "Aritmetica.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


